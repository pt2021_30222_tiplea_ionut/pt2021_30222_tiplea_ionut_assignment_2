package Presentation;

import javax.swing.*;
import java.awt.event.*;
import Thread.*;
class GUIThread implements ActionListener {
    private static JFrame frame;
    private static JPanel p = new JPanel();
    private static JTextField input1;
    private static JTextField input2;
    private static JTextField input3;
    private static JTextField input4;
    private static JTextField input5;
    private static JTextField input6;
    private static JTextField input7;
    private static JTextArea output;
    private static JLabel label1;
    private static JLabel label2;
    private static JLabel label3;
    private static JLabel label4;
    private static JLabel label5;
    private static JLabel label6;
    private static JLabel label7;
    private static JLabel label8;

    private static JButton button;
    public GUIThread(){
        frame = new JFrame("QueueSimulator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,300);
        label1 = new JLabel("NbClients:");
        label2 = new JLabel("NbQueues:");
        label3 = new JLabel("minTArrival");
        label4 = new JLabel("maxTArrival");
        label5 = new JLabel("minTService");
        label6 = new JLabel("maxTService");
        label7 = new JLabel("maxTSim");
        label8 = new JLabel("SimulationArea");
        button = new JButton("=");
        input1 = new JTextField(5);
        input2 = new JTextField(5);
        input3 = new JTextField(5);
        input4 = new JTextField(5);
        input5 = new JTextField(5);
        input6 = new JTextField(5);
        input7 = new JTextField(5);
        output = new JTextArea(10,5);
        label1.setVisible(true);
        label2.setVisible(true);
        label3.setVisible(true);
        label4.setVisible(true);
        label5.setVisible(true);
        label6.setVisible(true);
        label7.setVisible(true);
        button.addActionListener(this);

        p.add(label1);
        p.add(input1);
        p.add(label2);
        p.add(input2);
        p.add(label3);
        p.add(input3);
        p.add(label4);
        p.add(input4);
        p.add(label5);
        p.add(input5);
        p.add(label6);
        p.add(input6);
        p.add(label7);
        p.add(input7);
        p.add(button);
        p.add(label8);
        p.add(output);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        frame.getContentPane().add(p);
    }
    public void actionPerformed(ActionEvent e) {
        int n,q,minTA,maxTA,minTS,maxTS,maxT;
        if(e.getSource() == button) {
            try {
                n = input1.getText() == "" ? 0 : Integer.parseInt(input1.getText());
                q = input2.getText() == "" ? 0 : Integer.parseInt(input2.getText());
                minTA = input3.getText() == "" ? 0 : Integer.parseInt(input3.getText());
                maxTA = input4.getText() == "" ? 0 : Integer.parseInt(input4.getText());
                minTS = input5.getText() == "" ? 0 : Integer.parseInt(input5.getText());
                maxTS = input6.getText() == "" ? 0 : Integer.parseInt(input6.getText());
                maxT = input7.getText() == "" ? 0 : Integer.parseInt(input7.getText());
                ServerThread t = new ServerThread(q,n,minTA,maxTA,minTS,maxTS,maxT,output, p);
                t.start();
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(frame, "Invalid Input");
            }
        }
        p.updateUI();
    }
    public static void main(String args[]){
        GUIThread myGUI = new GUIThread();
        GUIThread.frame.setVisible(true);
    }
}
