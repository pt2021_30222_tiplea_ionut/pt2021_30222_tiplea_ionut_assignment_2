package Thread;

import Business.Client;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ServerThread extends Thread {
    private int nrThreads;
    private int t = 0;
    private int nr, tminA, tmaxA, tminS, tmaxS, maxT;
    private String toGui = "";
    private final JTextArea text;
    private final JPanel panel ;
    public ServerThread(int nrThreads,int nr, int tminA, int tmaxA, int tminS, int tmaxS,int maxT,JTextArea text,JPanel p) {
        this.nrThreads = nrThreads;
        this.nr = nr;
        this.tminA = tminA;
        this.tmaxA = tmaxA;
        this.tminS = tminS;
        this.tmaxS = tmaxS;
        this.maxT = maxT;
        this.text = text;
        this.panel = p;
    }

    public FileWriter initFile(String name, File myFile){
        FileWriter fw = null;
        try {
            myFile.createNewFile();
            fw = new FileWriter(name);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return fw;
    }
    public ArrayList<MyThread> createThreadList(int nrThreads){
        ArrayList<MyThread> list = new ArrayList<MyThread>(0);
        for(int i = 0; i < nrThreads; i ++){
            list.add(new MyThread(i));
        }
        for(MyThread t : list){
            t.start();
        }
        return list;
    }
    public void stopThreads(ArrayList<MyThread> list){
        for(MyThread th : list){
            th.stop();
        }
    }
    public String write( int t, ArrayList<MyThread> list,  ArrayList<Client> waitList, FileWriter fw) throws IOException{
        String s = "";
        s += "Time " +  t + "\n";
        s+= "Waiting Clients : ";
        fw.write("Time " +  t + "\n");
        fw.write("Waiting Clients : ");
        for(Client c : waitList){
            s+= c + ";";
            fw.write(c + ";");
        }
        s+= "\n";
        s+= MyThread.getStatus(list);
        fw.write("\n");
        fw.write(MyThread.getStatus(list));
        return s;
    }
    public int nrOfClients( ArrayList<MyThread> list,ArrayList<Client> waitList ){
        int rez = 0;
        for(MyThread e: list){
            rez += e.getC().size();
        }
        rez += waitList.size();
        return rez;
    }
    public String getToGui(){
        return toGui;
    }
    public int getT(){
        return t;
    }
    public void run(){
        File myFile = new File("log.txt");
        FileWriter fw = initFile("log.txt",myFile);
        ArrayList<MyThread> list = createThreadList(nrThreads);
        MyThread p;
        ArrayList<Client> waitList = Client.randomClientGenerator(nr,tminA,tmaxA,tminS,tmaxS);
        int avgWaitTime = 0;
        int avgServiceTime = 0;
        int peakHour = 0;
        int clientsPeak = 0;
        try {
            while (nrOfClients(list,waitList) > 0 && t <= maxT) {
                Thread.sleep(1000);
                ArrayList<Client> removables = new ArrayList<>(0);
                for (Client c : waitList) {
                    if (c.getArrivalTime() == t) {
                        p = MyThread.getMinThread(list);
                        p.getC().add(c);
                        avgWaitTime += p.getServiceTime();//the time it takes till this client gets served
                        p.setServiceTime(p.getServiceTime() + c.getServiceTime());
                        avgServiceTime += p.getServiceTime(); // the time it takes till this client gets served
                        removables.add(c);
                    }
                }
                for (Client c : removables) {
                    waitList.remove(c);
                }
                if(clientsPeak < MyThread.getNbClients(list)){
                    peakHour = t;
                    clientsPeak =  MyThread.getNbClients(list);
                }
                toGui = write(t++, list, waitList, fw);
                text.setText(toGui);
                panel.updateUI();
                System.out.println(t);
            }
            stopThreads(list);
            fw.write("Avg WaitTime: " + 1.0*avgWaitTime/nr + "\n");
            fw.write("Avg ServiceTime: " + 1.0*avgServiceTime/nr + "\n");
            fw.write("Peak Hour " + peakHour + "\n");
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
