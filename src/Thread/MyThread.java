package Thread;

import Business.Client;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class MyThread extends Thread{
    private Queue<Client> q = new LinkedList<>();
    private int id;
    private int serviceTime = 0;
    public MyThread(int id){
        this.id = id;
        serviceTime = 0;
    }
    @Override
    public void run(){
        int st;
        while(true){
            if(q.peek() != null)
                st = q.peek().getServiceTime();
            else
                st = 1;
            if(st == 0){
                q.remove();
            }
            else {
                if (q.peek() != null) {
                    q.peek().setServiceTime(st - 1);
                    serviceTime --;
                }
            }
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public String toString(){
        String s = "";
        if(q.isEmpty()){
            s += id + ": Closed\n";
        }
        else
            s += id + ": " + q + "\n";
        return s;
    }
    public static String getStatus(ArrayList<MyThread> list){
        String s = "";
        for(MyThread e : list){
            s += e;
        }
        return s;
    }
    public Queue<Client> getC() {
        return q;
    }
    public static MyThread getMinThread(ArrayList<MyThread> myList){
        MyThread p = myList.get(0);
        int minST = 100;
        for(MyThread t : myList){
            if(t.serviceTime < minST){
                minST = t.serviceTime;
                p = t;
            }
        }
        return p;
    }
    public int getServiceTime() {
        return serviceTime;
    }
    public void setServiceTime(int newST){
        this.serviceTime = newST;
    }
    public static int getNbClients(ArrayList<MyThread> list){
        int rez = 0;
        for(MyThread l : list){
            rez += l.q.size();
        }
        return rez;
    }
}
