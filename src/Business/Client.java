package Business;

import java.util.ArrayList;

public class Client {
    private int ID;
    private int arrivalTime;
    private int serviceTime;
    public Client(){
        this.ID = 0;
        this.arrivalTime = 0;
        this.serviceTime = 0;
    }
    public Client(int ID , int at, int st){
        this.ID = ID;
        this.arrivalTime = at;
        this.serviceTime = st;
    }
    public int getID() {
        return ID;
    }
    public void setID(int ID) {
        this.ID = ID;
    }
    public int getArrivalTime() {
        return arrivalTime;
    }
    public int getServiceTime() {
        return serviceTime;
    }
    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }
    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    @Override
    public String toString(){
        return "(" + ID + "," + arrivalTime + "," + serviceTime + ")";
    }
    public static ArrayList<Client> randomClientGenerator(int n,int tminA,int tmaxA,int tminS,int tmaxS){
        ArrayList<Client> p = new ArrayList<>(0);
        for(int i = 0; i < n; i ++){
            double f1 = Math.random()/Math.nextDown(1.0);
            double f2 = Math.random()/Math.nextDown(1.0);
            p.add(new Client(i+1, (int)(tminA*(1-f1)+ tmaxA*f1),(int)(tminS*(1-f2)+ tmaxS*f2)));
        }
        return p;
    }
}